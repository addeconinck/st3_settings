import sublime
import sublime_plugin
import os
import time
supported_extensions = ['.c', '.cpp', '.h', '.hh', '.hpp']

class FormatFolderCommand(sublime_plugin.TextCommand):
	
	def process_dir(self, dir, paths):
		for file in os.listdir(dir):
			path = os.path.join(dir, file)
			if os.path.isdir(path):
				self.process_dir(path, paths)
			elif os.path.isfile(path):  
				self.process_file(path, paths)
			else:
				pass

	def process_file(self, file, paths):
		file_name, file_extension = os.path.splitext(file)
		if(file_extension in supported_extensions):
			paths.append(file)

	def run(self, edit, **kwargs): 
		paths = []
		for key, dirs in kwargs.items():
			for dir in dirs:
				self.process_dir(dir, paths)
		sublime.set_timeout_async(lambda:self.format(paths), 0)

	def format(self, paths):
		for i,path in enumerate(paths):
			view = sublime.active_window().open_file(path)
			while(view.is_loading()):
				time.sleep(0.1)
			view.run_command('astyleformat')
			view.run_command('save')
			sublime.active_window().run_command('close_file')
			sublime.active_window().status_message("Formatting Files [{}/{}] ...".format(i, len(paths)))
		
	# only applicable to folders
	def is_visible(self, dirs):
		if(len(dirs) == 0):
			return False
		else:
			return True